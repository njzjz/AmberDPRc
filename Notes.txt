Jun  7, 2022
------------

    TJG: Added amber20ml_src.tar.bz2 from AmberTools20mlipi.07jun22.tar.bz2 corresponding to riesling:Programs/Amber18 branch amber20ml commit e1e7df577020a9defa0129ab349c35747197a13b
    
    TJG: Added InstallDeepMDKit.py from riesling:Programs/deepmdkit_distrip branch master commit 2f648f22e2e3ae8d475de356db85cd682ef620fb

    TJG: Added draft of INSTALL.sh; Jinzhe please fill-in instructions for using InstallDeepMDKit.py

    TJG: Added several mdin examples to examples/AmberInputQuickReference
    
