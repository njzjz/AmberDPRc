The amber20ml_src.tar.bz2 package contains a modified version of AmberTools
that provides support for DPRc corrections to QM and QM/MM interactions.

To compile AmberTools, you'll need to first install DeepMDKit and
the TensorFlow (including its C++-bindings). There is a
InstallDeepMDKit.py script to help you install these dependencies.
Instructions for installing DeepMDKit can be found on its project
page: https://github.com/deepmodeling/deepmd-kit
And instructions for using InstallDeepMDKit.py are provided
below.

INSTALLING AMBERTOOLS
=====================

Once you have installed DeepMDKit and TensorFlow libraries and
headers into /path/to/deepmdkit/lib and /path/to/deepmdkit/include,
you'll need to export an environmental variable so AmberTools
can locate them.

    export DEEMDHOME=/path/to/deepmdkit

Next, unpack AmberTools.

    tar -xjf amber20ml_src.tar.bz2
    cd amber20ml_src
    export AMBERHOME=${PWD}

For serial CPU support using the GCC compilers, configure with:

    ./configure --skip-python -noX11 -ml gnu

For parallel CPU support using the GCC compilers, configure with:

    ./configure --skip-python -noX11 -ml -mpi gnu

For serial GPU support using the GCC compilers, you'll first
need to set CUDA_HOME and then configure with:

    export CUDA_HOME=/path/to/cuda
    ./configure --skip-python -noX11 -ml -mlcuda gnu


The configure script may issue warnings about how the
configuration process is being deprecated in favor of cmake;
you should ignore these warnings.

Source the amber.sh environmental variables and build
the executable.

    source ${AMBERHOME}/amber.sh
    make install



INSTALLING DEEPMDKIT
====================

Required dependencies:
- [Python](https://www.python.org/) >=3.6
- [NumPy](https://numpy.org/)
- [GCC](https://gcc.gnu.org/) (gcc/g++) >=5
- [git](https://git-scm.com/) >=1.8.5
- [CMake](https://cmake.org/) >=3.12

For GPU builds, CUDA and cuDNN are required:
- [CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit-archive)
- [cuDNN](https://developer.nvidia.com/rdp/cudnn-archive)


The following dependency has been tested:
- Required NVIDIA driver version: 450.80.02
- [CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit-archive) version: 11.6.2
- [cuDNN](https://developer.nvidia.com/rdp/cudnn-archive) version: 8.4.0
- [GCC](https://gcc.gnu.org/) versions: 9.3, 10.2, 11.2


To install DeePMD-kit and the TensorFlow dependencies,

```sh
./InstallDeepMDKit.py --prefix /path/to/deepmdkit
```

DeePMD-kit will be installed to `/path/to/deepmdkit`.

If one want to enable GPU builds, add `--cuda`, `--cuda-path`, `--cudnn-path` options:

```sh
./InstallDeepMDKit.py --prefix /path/to/deepmdkit --cuda --cuda-path /path/to/cuda --cudnn-path /path/to/cudnn
```

See more options,
```sh
./InstallDeepMDKit.py -h
```
